---
features:
  secondary:
  - name: "List repository tags with the new container registry API"
    available_in: [free, silver, gold]
    documentation_link: 'https://docs.gitlab.com/ee/api/container_registry.html#list-registry-repository-tags'
    reporter: trizzi
    stage: package
    categories:
    - 'Container Registry'
    epic_url: 'https://gitlab.com/groups/gitlab-org/-/epics/10208'
    description: |
      Previously, the container registry relied on the Docker/OCI [listing image tags registry API](https://gitlab.com/gitlab-org/container-registry/-/blob/5208a0ce1600b535e529cd857c842fda6d19ad59/docs/spec/docker/v2/api.md#listing-image-tags) to display tags in GitLab. This API had significant performance and discoverability limitations.

      This API performed slowly because the number of network requests against the registry scaled with the number of tags in the tags list. In addition, because the API didn't track publish time, the published timestamp was often incorrect. There were also limitations when displaying images based on Docker manifest lists or OCI indexes, such as for multi-architecture images.
      
      To address these limitations, we introduced a new registry [list repository tags API](https://gitlab.com/gitlab-org/container-registry/-/blob/5208a0ce1600b535e529cd857c842fda6d19ad59/docs/spec/gitlab/api.md#list-repository-tags). In GitLab 16.10, we've completed the migration to the new API. Now, whether you use the UI or the REST API, you can expect improved performance, accurate publication timestamps, and robust support for multi-architecture images.

      This improvement is available only on GitLab.com. Self-managed support is blocked until the next-generation container registry is generally available. To learn more, see [issue 423459](https://gitlab.com/gitlab-org/gitlab/-/issues/423459).
