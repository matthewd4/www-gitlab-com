---
release_number: "16.11" # version number - required
title: "GitLab 16.11 released with GitLab Duo Chat general availability" # short title (no longer than 62 characters) - required
author: Loryn Bortins # author name and surname - required
author_gitlab: lbortins # author's gitlab.com username - required
image_title: '/images/16_11/16_11-cover-image.png' # cover image - required
description: "GitLab 16.11 released with GitLab Duo Chat general availability, CI/CD Components general availability, Product Analytics general availability, Security Policy Scopes, and much more!" # short description - required
twitter_image: '/images/16_11/16_11-cover-image.png' # required - copy URL from image title section above
categories: releases # required
layout: release # required
featured: yes
rebrand_cover_img: true

# APPEARANCE
# header_layout_dark: true #uncomment if the cover image is dark
# release_number_dark: true #uncomment if you want a dark release number
# release_number_image: "/images/X_Y/X_Y-release-number-image.svg" # uncomment if you want a svg image to replace the release number that normally overlays the background image

---

<!--
This is the release blog post file. Add here the introduction only.
All remaining content goes into data/release-posts/.

**Use the merge request template "Release-Post", and please set the calendar due
date for each stage (general contributions, review).**

Read through the Release Posts Handbook for more information:
https://about.gitlab.com/handbook/marketing/blog/release-posts/#introduction
-->

Today, we are excited to announce the release of GitLab 16.11 with [GitLab Duo Chat general availability](#gitlab-duo-chat-now-generally-available), [Product Analytics general availability](#understand-your-users-better-with-product-analytics), [Security policy scopes](#security-policy-scopes), and much more!

These are just a few highlights from the 40+ improvements in this release. Read on to check out all of the great updates below.

To the wider GitLab community, thank you for the 190+ contributions you provided to GitLab 16.11!
At GitLab, [everyone can contribute](https://about.gitlab.com/community/contribute/) and we couldn't have done it without you!

To preview what's coming in next month’s release, check out our [Upcoming Releases page](/direction/kickoff/), which includes our 17.0 release kickoff video.
